# liquigo: Liquibase by golang

liquigo是一个参考[Liquibase中间件](https://liquibase.org/)的golang开发的数据库版本管理工具，实现了大多数以xml格式配置的常用语法，支持的数据库类型和语法参见如下表格：

|Liquibase语法（仅支持xml格式）|MySQL / TiDB / OceanBase|MariaDB|openGauss / PostgreSQL|KingbaseES|达梦|SQLite|Oracle|SQL Server|
|-|-|-|-|-|-|-|-|-|
|变更集：changeSet|√|√|√|√|√|√|√|√|
|变更集注释：comment|√|√|√|√|√|√|√|√|
|创建表：createTable|√|√|√|√|√|√|√|√|
|删除表：dropTable|√|√|√|√|√|√|√|√|
|重命名表：renameTable|√|√|√|√|√|-|√|√|
|添加列：addColumn|√|√|√|√|√|-|√|√|
|删除列：dropColumn|√|√|√|√|√|√|√|√|
|重命名列：renameColumn|√|√|√|√|√|-|√|√|
|变更列：modifyDataType|√|√|√|√|√|-|√|√|
|删除默认值：dropDefaultValue|√|√|√|√|√|-|√|√|
|创建视图：createView|√|√|√|√|√|√|√|√|
|删除视图：dropView|√|√|√|√|√|√|√|√|
|创建索引：createIndex|√|√|√|√|√|√|√|√|
|删除索引：dropIndex|√|√|√|√|√|√|√|√|
|属性替换${}|√|√|√|√|√|√|√|√|
|执行SQL语句：sql|√|√|√|√|√|√|√|√|
|执行SQL文件：sqlFile|√|√|√|√|√|√|√|√|
|前置条件：preConditions|√|√|√|√|√|√|√|√|
|前置条件与或非：not,and,or|√|√|√|√|√|√|√|√|
|前置条件存在判断：tableExists,viewExists|√|√|√|√|√|√|√|√|
|前置条件存在判断：columnExists,sqlCheck|√|√|√|√|√|√|√|√|
|前置条件存在判断：indexExists|√|√|√|√|√|×|√|√|
|事务回滚：rollback|√|√|√|√|√|√|√|√|
|开发调试用停止：stop|√|√|√|√|√|√|√|√|

测试过的数据库系统版本有：

openGauss V2.1.0 (兼容PostgreSQL模式)

OceanBase V3.1.1 (兼容MySQL模式)

MySQL V5.7.27 / MySQL V8.0.28

TiDB V5.4.0

MariaDB V10.3.34

PostgreSQL V12.10

KingbaseES V008R006

达梦 V8.1.2.2

SQLite V3.38

Oracle Express Edition V21c

SQL Server V2019

具体使用请参考app.yml配置文件以及entry-\*.xml和db目录下的若干xml配置文件。