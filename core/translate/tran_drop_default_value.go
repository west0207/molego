// Copyright 2022 The Molego Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	. "gitee.com/west0207/molego/core/log"
	utils "gitee.com/west0207/molego/core/utils"

	etree "gitee.com/west0207/etree"
)

// 获取删除字段默认值的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// dbmsVersion *string 数据库系统版本，例如：5
// dropDefaultValueEle *etree.Element 删除字段默认值的Entity标签
func GetDropDefaultValueSql(dbmsName *string, dbmsVersion *string, dropDefaultValueEle *etree.Element) (string, error) {
	return getDefaultDropDefaultValueSql(dbmsName, dbmsVersion, dropDefaultValueEle)
}

// <dropDefaultValue columnDataType="varchar(250)" columnName="detail" tableName="test_role"/>
// 如下为通用模式，例如PostgreSQL v12+ ...
// alter table test_column_type alter column type_text drop default;
// 如下仅适用于mysql
// alter table test_role modify column detail varchar(250);
// sqlite数据库不支持删除字段默认值操作dropDefaultValue
// 【oracle】
// alter table t_default_test modify id default null;
//
// 【mssql】
// 仅有条件不支持，即默认值约束名称是固定格式: dfc_{tableName}_{columnName}
// alter table test_role drop constraint if exists dfc_test_role_update_user;
// 否则不支持，如下原因：
// 需要先查询出来默认值约束的名称，再删除，处理略微复杂，暂时不做支持
// 【1：查询出来默认值约束的名称】
// select a.name default_constraint_name from (
// 	 select name,id cdefault,parent_obj from sysobjects
// 	 where xtype = 'D'
// 	 and parent_obj in (
// 	   select id from sysobjects where name = 'test_role'
// 	 )
// ) a,
// (
// 	 select name, id parent_obj, cdefault from syscolumns
// 	 where name = 'app_id' and cdefault !='0'
// ) b
// where a.cdefault = b.cdefault
// and a.parent_obj = b.parent_obj
// 【2：删除该默认值约束】
// alter table test_role drop constraint {default_constraint_name};
//
func getDefaultDropDefaultValueSql(dbmsName *string, dbmsVersion *string, dropDefaultValueEle *etree.Element) (string, error) {
	tableName := dropDefaultValueEle.SelectAttrValue("tableName", utils.EMPTY)
	SetPropertyValue(dbmsName, &tableName)

	columnName := dropDefaultValueEle.SelectAttrValue("columnName", utils.EMPTY)
	SetPropertyValue(dbmsName, &columnName)

	switch *dbmsName {
	case SQLite:
		Sug.Errorf("sqlite数据库不支持删除字段默认值操作dropDefaultValue: %v", tableName)
		return "-- SQLite database does not support drop default value of table fields" + utils.LF, nil
	case MySQL, MariaDB, TiDB:
		// alter table test_role modify column detail varchar(250);
		ctype := dropDefaultValueEle.SelectAttrValue("columnDataType", utils.EMPTY)
		SetPropertyValue(dbmsName, &ctype)
		SetColumnType(dbmsName, &ctype)
		sql := "alter table " + tableName + " modify column " + columnName + utils.SPACE + ctype + utils.SEMICOLON + utils.LF
		return sql, nil
	case PostgreSQL, Kingbase:
		// alter table test_column_type alter column type_text drop default;
		sql := "alter table " + tableName + " alter column " + columnName + " drop default" + utils.SEMICOLON + utils.LF
		return sql, nil
	case Oracle, Dameng:
		// alter table t_default_test modify id default null;
		sql := "alter table " + tableName + " modify " + columnName + " default null" + utils.SEMICOLON + utils.LF
		return sql, nil
	case MsSQLServer:
		// alter table test_role drop constraint if exists dfc_test_role_update_user;
		sql := "alter table " + tableName + " drop constraint if exists dfc_" + tableName + "_" + columnName + utils.SEMICOLON + utils.LF
		return sql, nil
	default:
		// alter table test_column_type alter column type_text drop default;
		sql := "alter table " + tableName + " alter column " + columnName + " drop default" + utils.SEMICOLON + utils.LF
		return sql, nil
	}
}
