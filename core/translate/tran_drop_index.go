// Copyright 2022 The Molego Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	utils "gitee.com/west0207/molego/core/utils"

	etree "gitee.com/west0207/etree"
)

// 获取删除索引的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// dropIndexEle *etree.Element 删除索引的Entity标签
func GetDropIndexSql(dbmsName *string, dropIndexEle *etree.Element) (string, error) {
	return getDefaultDropIndexSql(dbmsName, dropIndexEle)
}

// alter table test_role drop index uk_test_role_name -- mysql
// drop index uk_test_role_uuid
func getDefaultDropIndexSql(dbmsName *string, dropIndexEle *etree.Element) (string, error) {
	indexName := dropIndexEle.SelectAttrValue("indexName", utils.FALSE)
	SetPropertyValue(dbmsName, &indexName)
	tableName := dropIndexEle.SelectAttrValue("tableName", utils.EMPTY)
	SetPropertyValue(dbmsName, &tableName)
	var sql string
	switch *dbmsName {
	case MySQL, MariaDB, TiDB:
		sql = "alter table " + tableName + " drop index " + indexName + ";" + utils.LF
	case MsSQLServer:
		// drop index uk_test_role_name on test_role;
		sql = "drop index " + indexName + " on " + tableName + ";" + utils.LF
	default:
		sql = "drop index " + indexName + ";" + utils.LF
	}

	return sql, nil
}
