// Copyright 2022 The Molego Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	"strings"

	. "gitee.com/west0207/molego/core/log"
	utils "gitee.com/west0207/molego/core/utils"

	etree "gitee.com/west0207/etree"
)

// 获取重命名表字段的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// dbmsVersion *string 数据库系统版本，例如：5
// renameColumnEle *etree.Element 重命名表字段的Entity标签
func GetRenameColumnSql(dbmsName *string, dbmsVersion *string, renameColumnEle *etree.Element) (string, error) {
	return getDefaultRenameColumnSql(dbmsName, dbmsVersion, renameColumnEle)
}

// <renameColumn columnDataType="varchar(36)"
// 		newColumnName="new_app_id"
// 		oldColumnName="app_id"
// 		remarks="app id"
// 		tableName="test_t04"/>
// 如下为通用模式，例如mysql v8+, PostgreSQL v12+, Oracle...
// alter table test_t01 rename column detail to detail_new;
// 如下仅适用于mysql v5
// alter table test_t01 change column detail detail_new varchar(250)
// sqlite数据库不支持重命名表字段操作renameColumn
func getDefaultRenameColumnSql(dbmsName *string, dbmsVersion *string, renameColumnEle *etree.Element) (string, error) {
	tableName := renameColumnEle.SelectAttrValue("tableName", utils.EMPTY)
	SetPropertyValue(dbmsName, &tableName)

	if *dbmsName == SQLite {
		Sug.Errorf("sqlite数据库不支持重命名表字段操作renameColumn: %v", tableName)
		return "-- SQLite database does not support renaming table fields" + utils.LF, nil
	}

	oldColumnName := renameColumnEle.SelectAttrValue("oldColumnName", utils.EMPTY)
	SetPropertyValue(dbmsName, &oldColumnName)
	newColumnName := renameColumnEle.SelectAttrValue("newColumnName", utils.EMPTY)
	SetPropertyValue(dbmsName, &newColumnName)
	ctype := renameColumnEle.SelectAttrValue("columnDataType", utils.EMPTY)
	SetPropertyValue(dbmsName, &ctype)
	SetColumnType(dbmsName, &ctype)
	cremarks := renameColumnEle.SelectAttrValue("remarks", utils.EMPTY)
	cremarks = strings.ReplaceAll(cremarks, utils.SINGLE_QUOTE, utils.EMPTY)

	if (*dbmsName == MySQL && *dbmsVersion == "5") || *dbmsName == MariaDB || *dbmsName == TiDB {
		// mysql v5 的rename column语句特殊
		// alter table test_t01 change column detail_old detail_new varchar(250) comment 'detail info';
		if utils.EMPTY == cremarks {
			sql := "alter table " + tableName + " change column " + oldColumnName + utils.SPACE + newColumnName + utils.SPACE + ctype + utils.SEMICOLON + utils.LF
			return sql, nil
		}
		sql := "alter table " + tableName + " change column " + oldColumnName + utils.SPACE + newColumnName + utils.SPACE + ctype + " comment '" + cremarks + "'" + utils.SEMICOLON + utils.LF
		return sql, nil
	}

	if *dbmsName == MsSQLServer {
		// exec sp_rename 'test_column_type_new.type_timestamp', 'type_timestamp_new', 'column';
		sql := "exec sp_rename '" + tableName + "." + oldColumnName + "', '" + newColumnName + "', 'column';"
		return sql, nil
	}

	// alter table test_t01 rename column detail_old to detail_new;
	sql := "alter table " + tableName + " rename column " + oldColumnName + " to " + newColumnName + utils.SEMICOLON + utils.LF
	if utils.EMPTY != cremarks {
		// 有字段注释
		switch *dbmsName {
		case SQLite:
			// sqlite数据库添加新列不支持注释
		case Kingbase, PostgreSQL, Dameng, Oracle:
			// comment on column test_t04.first_login_time is '首次登录时间, yyyy-MM-dd HH:mm:ss';
			comment := "comment on column " + tableName + "." + newColumnName + " is '" + cremarks + "';" + utils.LF
			sql = sql + comment
		default:
			// mysql,mariadb,h2
		}
	}
	return sql, nil
}
