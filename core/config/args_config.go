// Copyright 2022 The Molego Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

// 命令行参数[-d]空转，仅输出sql文本
var DryRun bool = false

// 命令行参数[-v]显示版本号(utils.MOLEGO_VERSION)
var Version bool = false

// 命令行参数[-x]显示中间件xorm的Debug信息
var XormLog bool = false

// molego版本号
var MolegoVersion string = "0.2.17"

// 默认的yaml配置文件
var YamlConfig string = "app.yml"
