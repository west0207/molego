// Copyright 2022 The Molego Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package utils

const (
	AMPERSAND         = "&"
	AND               = "and"
	AT                = "@"
	STAR              = "*"
	BACK_SLASH        = "\\"
	COLON             = ":"
	COMMA             = ","
	DASH              = "-"
	DOLLAR            = "$"
	DOT               = "."
	DOTDOT            = ".."
	DOT_CLASS         = ".class"
	DOT_JAVA          = ".java"
	EMPTY             = ""
	EQUALS            = "="
	FALSE             = "false"
	SLASH             = "/"
	HASH              = "#"
	HAT               = "^"
	LEFT_BRACE        = "{"
	LEFT_BRACKET      = "("
	LEFT_CHEV         = "<"
	NEWLINE           = "\n"
	N                 = "n"
	NO                = "no"
	NULL              = "null"
	OFF               = "off"
	ON                = "on"
	PERCENT           = "%"
	PIPE              = "|"
	PLUS              = "+"
	QUESTION_MARK     = "?"
	EXCLAMATION_MARK  = "!"
	QUOTE             = "\""
	RETURN            = "\r"
	TAB               = "\t"
	RIGHT_BRACE       = "}"
	RIGHT_BRACKET     = ")"
	RIGHT_CHEV        = ">"
	SEMICOLON         = ";"
	SINGLE_QUOTE      = "'"
	BACKTICK          = "`"
	SPACE             = " "
	TILDA             = "~"
	LEFT_SQ_BRACKET   = "["
	RIGHT_SQ_BRACKET  = "]"
	TRUE              = "true"
	UNDERSCORE        = "_"
	UTF_8             = "UTF-8"
	US_ASCII          = "US-ASCII"
	ISO_8859_1        = "ISO-8859-1"
	Y                 = "y"
	YES               = "yes"
	ONE               = "1"
	ZERO              = "0"
	DOLLAR_LEFT_BRACE = "${"
	CRLF              = "\r\n"
	LF                = NEWLINE

	HTML_NBSP  = "&nbsp;"
	HTML_AMP   = "&amp"
	HTML_QUOTE = "&quot;"
	HTML_LT    = "&lt;"
	HTML_GT    = "&gt;"

	UNKNOWN = "unknown"
	ALL     = "all"
	NONE    = "none"

	INT_ONE      = 1
	INT_ZERO     = 0
	ONE_HUNDRED  = 100
	ONE_THOUSAND = 1000
	TEN_THOUSAND = 10000

	SUCCESS = "success"
	FAILURE = "failure"

	// 该changeSet可以运行
	CS_RUN = "run"
	// changeSet已经运行过，并且sha256摘要值未变更，无需再运行
	// CS_RAN = "ran"
	// changeSet被首次执行
	CS_EXECUTED = "executed"
	// changeSet变更后再次执行（changeSet.runOnChange=true）
	CS_RERAN = "reran"
	// changeSet冲突，即changeSet.runOnChange=false时被修改，前后的sha256摘要值不相等
	CS_CONFLICT = "conflict"

	// 日期时间格式：2006-01-02 15:04:05
	YMDHMS = "2006-01-02 15:04:05"

	// 逻辑运算与：and
	LO_AND = "&&"
	// 逻辑运算或：or
	LO_OR = "||"
	// 逻辑运算非：not
	LO_NOT = "!"

	// preConditions onFail/onError : HALT 立即停止执行整个更改日志[默认]
	PC_HALT = "HALT"
	// preConditions onFail/onError : CONTINUE 跳过该changeSet，下次更新时将再次尝试执行更改集。
	PC_CONTINUE = "CONTINUE"
	// preConditions onFail/onError : MARK_RAN 跳过该changeSet，但将其标记为已执行。
	PC_MARK_RAN = "MARK_RAN"
	// preConditions onFail/onError : WARN 输出警告并继续照常执行该changeSet。
	PC_WARN = "WARN"

	// 空字符：空格、换行、Tab
	BLANK_CHARS = " \n	"
)
