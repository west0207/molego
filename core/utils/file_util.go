// Copyright 2022 The Molego Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package utils

import (
	"os"

	. "gitee.com/west0207/molego/core/log"
)

// 获取指定文件的字节数组
// @param filePath *string 文件路径
// @return ([]byte, error)
func GetFileBytes(filePath *string) ([]byte, error) {
	file, err := os.Open(*filePath)
	if err != nil {
		Sug.Error(err)
		return nil, err
	}
	defer file.Close()

	fileinfo, err := file.Stat()
	if err != nil {
		Sug.Error(err)
		return nil, err
	}

	filesize := fileinfo.Size()
	buffer := make([]byte, filesize)

	bytesread, err := file.Read(buffer)
	if err != nil {
		Sug.Error(err)
		Sugf.Error(*filePath+" bytes read: ", bytesread)
		return nil, err
	}
	return buffer, nil
}
