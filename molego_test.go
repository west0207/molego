// Copyright 2022 The Molego Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"strings"
	"testing"

	handle "gitee.com/west0207/molego/core/handle"
	. "gitee.com/west0207/molego/core/log"
)

func TestMain(m *testing.M) {
	InitLog()
	Sug.Infof("molego start")
	if err := handle.Handle("app_test.yml"); err != nil {
		if strings.HasPrefix(err.Error(), "molego stopped") {
			Sug.Info(err.Error())
		} else {
			Sug.Errorf("%v", err)
		}
	}
	Sug.Infof("molego end")
}
